# EEG Device Simulator #

## Brief description ##
This is a Windows based code meant to transmit EEG samples from the computer to another computer/micro controller system via Bluetooth-serial or serial link. This software can be used to simulate an EEG device for research purpose if an actual EEG device is unavailable. 

This software was employed to send EEG samples to a micro controller system with the same timing as that of an actual EEG device. In our study, a Bluetooth-serial module was used for wireless connectivity between the computer and the micro controller. When Bluetooth connectivity is established successfully between the computer and the Bluetooth module, a virtual COM port is spawned on the computer. This software communicates with that virtual COM port. If one intends to use an actual COM port, you simply have to choose that COM port in 'Step 1' of 'How to use this software'

## Data description ##

The data set used in the project is provided by Department of Medical Informatics, Institute for Biomedical Engineering, University of Technology Graz. (Gert Pfurtscheller) and is available on [Berlin Brain Computer interface website ]((http://www.bbci.de/competition/ii/)).

Each trial is 9 seconds long. During the initial 3 seconds, the test subject is asked to relax so the initial 3 seconds of EEG data is removed from the samples. The remaining 6 seconds of trial are of interest. Since the EEG sampling rate is 128Hz, the total number of samples per trial per EEG channel is 768 (128Hz x 6sec).

Each transmitted sample is comprised of two float numbers. These two float numbers represent the digitized value of that particular EEG sample captured from EEG channel 3 and EEG channel 4, respectively.

## How to set up this software ##
Microsoft .NET Framework is required to run this software; version 4.5 is recommended. This software doesn't require installation. You only need to download this repository, using either of the following methods. 'Method 1' is recommended if you are not comfortable working with git.

### Method 1: Download EEG Device Simulator as a ZIP archive ###

1- Download this repository as a ZIP archive using [this link](https://bitbucket.org/faisalmehmood59/eeg-device-simulator/get/af71d2b86542.zip). Your browser may raise a warning for downloading an archived executable file. You can ignore this warning.

2- Extract the archive.

3- You should be able to run the software 'EEG Device Simulator.exe'

### Method 2: Clone the git repository ###
To clone the repository follow these steps:

1- [Set up Git](https://confluence.atlassian.com/bitbucket/set-up-git-744723531.html) if you already haven't.

2- Open your Git command prompt.

3- Navigate to the directory where you want to copy the software.

4- Issue this command in the command prompt:

```
#!c#

git clone https://faisalmehmood59@bitbucket.org/faisalmehmood59/eeg-device-simulator.git

```

5- You should be able to run the software 'EEG Device Simulator.exe'.

## How to use this software ##

Note: This code uses Stopwatch component of Microsoft .NET Framework as a high-precision counter. This ensures accurate timing of transmission of EEG samples. 

1- Under "Select COM port" option, select the desired COM port and the baud-rate. The recommended baud-rate is 115200 bps. Click on "Connect"

2- Under "Select Data" option, choose the required data sample. There are 140 samples in all; 70 EEG trials in which the test subject imagined right movement and another 70 EEG trials during which the test subject imagined left movement. Click on "Load". The selected EEG trial will be plotted on the graph.

3- Under "Control" option, click on "Start". A progress bar will pop up showing the progress of transmission. The lower text box will display the value of each of the sample in 'float' notation. There are 768 samples in all.

Any data transmitted by the other end of the serial connection will be displayed in the upper text box.

## Contact the author ##
You can reach me at faisalmehmood59 <at> hotmail <dot> com.